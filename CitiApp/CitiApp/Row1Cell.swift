//
//  Row1Cell.swift
//  CitiApp
//
//  Created by Hitendra on 10/11/21.
//

import UIKit

class Row1Cell: UITableViewCell {

    @IBOutlet weak var weeklyGoalLabel: UILabel!
    @IBOutlet weak var dailyRecordLabel: UILabel!
    @IBOutlet weak var activeDaysLabel: UILabel!
    @IBOutlet weak var activityToDateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
