//
//  PandingCoworkerChallangesVC.swift
//  CitiApp
//
//  Created by Hitendra on 14/11/21.
//

import UIKit

struct Panding {
    var title : String?
    var date : String?
    
}

class PendingChallangeCell : UITableViewCell{
    
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblDate : UILabel!
    @IBOutlet weak var viewborder : UIView!

}

class PandingCoworkerChallangesVC: UIViewController {

    @IBOutlet weak var tblActive : UITableView!
    @IBOutlet weak var lblMostRecent : UILabel!

    var pandingArray = [Panding(title: "Brazil's Don't sit,Get it Challange 2", date: "Nov 8-Nov 14,2021"),Panding(title: "Brazil's Don't sit,Get it Challange 3", date: "Nov 12-Nov 16,2021")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }


}

extension PandingCoworkerChallangesVC : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pandingArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PendingChallangeCell", for: indexPath) as! PendingChallangeCell
        cell.lblTitle.text = self.pandingArray[indexPath.row].title
        cell.lblDate.text = self.pandingArray[indexPath.row].date
        if(indexPath.row == self.pandingArray.count-1){
            cell.viewborder.isHidden = true
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}
