//
//  ViewController.swift
//  CitiApp
//
//  Created by Hitendra on 10/11/21.
//

import UIKit

enum ChallangesRows : Int{
    case Image = 0
    case Row1 = 1
    case Row2 = 2
    case Row3 = 3 // for Register
}

class ViewController: UIViewController {

    @IBOutlet weak var tblView : UITableView!
    
    var rows : [ChallangesRows] = [.Image,.Row1,.Row2]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblView.register(
            UINib(nibName: "SectionImageCell",bundle: nil),
            forCellReuseIdentifier: "SectionImageCell"
        )
        tblView.register(
            UINib(nibName: "Row1Cell",bundle: nil),
            forCellReuseIdentifier: "Row1Cell"
        )

    }


}

extension ViewController : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rows.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let row = rows[indexPath.row] as ChallangesRows
        switch row {
        case .Image:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SectionImageCell", for: indexPath) as? SectionImageCell
            cell?.sectionImage.image = UIImage(named: "Header_Runner")
            return cell!
        case .Row1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Row1Cell", for: indexPath) as? Row1Cell
            return cell!
        default:
            let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
            cell.textLabel?.text = "\(indexPath.row)"
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let row = rows[indexPath.row] as ChallangesRows
        switch row {
        case .Image:
            return 300
        case .Row1:
            return 500
        default:
            return 40
        }
    }

}
