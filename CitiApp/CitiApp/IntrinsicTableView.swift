//
//  IntrinsicTableView.swift
//  Citi Live Well 2019 SV
//
//  Created by Noah Santorello on 10/9/18.
//  Copyright © 2018. All rights reserved.
//

import UIKit

class IntrinsicTableView : UITableView {
    
    override var contentSize:CGSize {
        didSet {
            self.invalidateIntrinsicContentSize()
        }
    }
    
    override var intrinsicContentSize: CGSize {
        self.layoutIfNeeded()
        return CGSize(width: UIView.noIntrinsicMetric, height: contentSize.height)
    }
    
}
