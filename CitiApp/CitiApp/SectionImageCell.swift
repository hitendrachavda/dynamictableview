//
//  SectionImageCell.swift
//  CitiApp
//
//  Created by Hitendra on 10/11/21.
//

import UIKit

class SectionImageCell: UITableViewCell {

    @IBOutlet weak var sectionImage : UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        sectionImage.contentMode = .scaleToFill
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
