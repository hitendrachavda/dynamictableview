//
//  ViewController2.swift
//  CitiApp
//
//  Created by Hitendra on 11/11/21.
//

import UIKit

class ViewController2: UIViewController {

    @IBOutlet weak var viewProgress : UIView!
    @IBOutlet weak var viewRegister : UIView!

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func hideShow(_ sender : UIButton){
        viewProgress.isHidden = !viewProgress.isHidden
        if(viewProgress.isHidden == true){
            viewRegister.isHidden = false
        }
        else{
            viewRegister.isHidden = true
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Ensure that child view controller autolayout works
        segue.destination.view.translatesAutoresizingMaskIntoConstraints = false
        if segue.identifier == "LandingMyProgress" {
        }
    }

}
