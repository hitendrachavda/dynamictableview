//
//  ActiveCoworkerChallangesVC.swift
//  CitiApp
//
//  Created by Hitendra on 14/11/21.
//

import UIKit

class ActiveChallangeCell : UITableViewCell{
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblDate : UILabel!
    @IBOutlet weak var viewborder : UIView?

}

class ActiveCoworkerChallangesVC: UIViewController {

    @IBOutlet weak var tblActive : UITableView!
    @IBOutlet weak var viewHeader : UIView!
    
    var pandingArray = [Panding(title: "Brazil's Don't sit,Get it Challange 2", date: "Nov 8-Nov 14,2021"),Panding(title: "Brazil's Don't sit,Get it Challange 3", date: "Nov 12-Nov 16,2021")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
}
extension ActiveCoworkerChallangesVC : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pandingArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "Cell")
        cell.textLabel?.text = "There is no challanges to display"
        if(self.pandingArray.count > 0){
            let pcell = tableView.dequeueReusableCell(withIdentifier: "ActiveChallangeCell", for: indexPath) as! ActiveChallangeCell
            pcell.lblTitle.text = self.pandingArray[indexPath.row].title
            pcell.lblDate.text = self.pandingArray[indexPath.row].date
            if(indexPath.row == self.pandingArray.count-1){
                pcell.viewborder?.isHidden = true
            }
            return pcell
        }
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(self.pandingArray.count > 0){
            return 70
        }
        return 40
    }
}
